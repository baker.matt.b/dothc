# dothc

The dothc.yml file contains the Cloudformation code used to complete this task.

Screenshots folder contains screenshots of this stack up and running in AWS.

Diagrams folder contains a network diagram and a screenshot of geolocation based traffic routing to individual load balancers in different availability zones.
